import datetime
import unittest

class TracksNSessions(object):
    def __init__(self, tracks_info):
        self.available_tracks = tracks_info
        self.tracks_bucket1 = []
        self.tracks_bucket2 = []
        self.tracks_bucket3 = []
        self.segregate_tracks()

    def segregate_tracks(self):
        """
        This function segregates given tracks into 3 different buckets based on duration.
        """
        for t in self.available_tracks:
            if t[1] == 60:
                self.tracks_bucket1.append(t)
        for t in self.available_tracks:
            if t[1] == 45:
                self.tracks_bucket2.append(t)
        for t in self.available_tracks:
            if t[1] == 30:
                self.tracks_bucket3.append(t)

    def choose_tracks(self, totaltime):
        """
        This function returns the combination of available tracks which cover totaltime, 
        using Greedy approach and recursion. First it'll try to fit large tracks 
        and then move to smaller tracks.
        This function is not very generic and assumes a certain track sizes. This question  is
        primarily NP-Hard and its solution doesn't exits in polynomial time. However, efficient and 
        probable solution can be achieved using recursion and backtracking. We recursively store 
        tracks on a stack and try to fill remaining time with remaining tracks. If we reach a dead
        end, we backtrack and pop out tracks from our stack and try other combinations sequentially. 
        Eventually, we'll find a combination that works, if it exists.
        This is a variation of change-making and multiple knapsack problem.
        """
        if totaltime == 0:
            return []
        if totaltime>0:
            if totaltime>=60 and len(self.tracks_bucket1)>0:
                item = self.tracks_bucket1.pop()
                return [item] + self.choose_tracks(totaltime-60)
            if totaltime>=90 and len(self.tracks_bucket2)>1:
                item1 = self.tracks_bucket2.pop()
                item2 = self.tracks_bucket2.pop()
                return [item1, item2] + self.choose_tracks(totaltime-90)
            if totaltime>=30 and len(self.tracks_bucket3)>0:
                item = self.tracks_bucket3.pop()
                return [item] + self.choose_tracks(totaltime-30)
        return []

    def assign_tracks(self):
        """
        Assigns tracks, which is then used by display_tracks function.
        """
        self.tracks1_1 = self.choose_tracks(180)
        self.tracks1_2 = self.choose_tracks(240)
        self.tracks2_1 = self.choose_tracks(180)
        self.tracks2_2 = self.choose_tracks(240)
        return [self.tracks_bucket1, self.tracks_bucket2, self.tracks_bucket3]

    def print_formatted_tracks(self, tracks, current_time):
        """
        Prints formatted tracks and return updated time.
        """
        for t in tracks:
            print "%s %s"%(current_time.strftime('%I:%M%p'), t[0])
            current_time += datetime.timedelta(0, t[1]*60) #multiplied to convert minutes to seconds.
        return current_time

    def display_tracks(self):
        """
        Driver function to print tracks and sessions. Keeps controls of break events like 
        Lunch and networking event.
        """
        print "Track 1:"
        current_time = datetime.datetime(2000,1,1,9,00,00) # we are only concerned with time
        current_time = self.print_formatted_tracks(self.tracks1_1, current_time)
        
        print "12:00PM Lunch"
        current_time += datetime.timedelta(0, 60*60)
        current_time = self.print_formatted_tracks(self.tracks1_2, current_time)
        print "05:00PM Networking Event"

        print "Track 2:"
        current_time = datetime.datetime(2000,1,1,9,00,00) # we are only concerned with time
        current_time = self.print_formatted_tracks(self.tracks2_1, current_time)
        
        print "12:00PM Lunch"
        current_time += datetime.timedelta(0, 60*60)
        current_time = self.print_formatted_tracks(self.tracks2_2, current_time)
        print "05:00PM Networking Event"

    def get_tracks_buckets(self):
        return [self.tracks_bucket1, self.tracks_bucket1, self.tracks_bucket1]

    def get_session_tracks(self):
        return [self.tracks1_1, self.tracks1_2, self.tracks2_1, self.tracks2_2]


##############################################################
############ class TracksNSessions definition ends ###########
##############################################################

class Testing(unittest.TestCase):
    def setUp(self):
        """
        Instantiating TracksNSessions class.
        """
        self.tns = TracksNSessions(available_tracks)
        self.tns.assign_tracks()
        self.tns.display_tracks()

    def testing_remaining_tracks_in_buckets(self):
        tracks = self.tns.get_tracks_buckets()
        self.assertEqual(tracks[0], [], "some tracks remaining in bucket 1")
        self.assertEqual(tracks[1], [], "some tracks remaining in bucket 2")
        self.assertEqual(tracks[2], [], "some tracks remaining in bucket 3")

    def testing_unfilled_sessions(self):
        tracks = self.tns.get_session_tracks()
        self.assertEqual(sum([t[1] for t in tracks[0]]), 180, "Track 1, before lunch is incomplete")
        self.assertEqual(sum([t[1] for t in tracks[1]]), 240, "Track 1, after lunch is incomplete")
        self.assertEqual(sum([t[1] for t in tracks[2]]), 180, "Track 2, before lunch is incomplete")
        self.assertEqual(sum([t[1] for t in tracks[3]]), 240, "Track 2, after lunch is incomplete")

    def tearDown(self):
        pass

#storing available tracks as a list of tuples
available_tracks = [
    ("Writing Fast Tests Against Enterprise Rails", 60),
    ("Overdoing it in Python", 45),
    ("Lua for the Masses", 30),
    ("Ruby Errors from Mismatched Gem Versions", 45),
    ("Common Ruby Errors", 45),
    ("Rails for Python Developers lightning", 60),
    ("Communicating Over Distance", 60),
    ("Accounting-Driven Development", 45),
    ("Woah", 30),
    ("Sit Down and Write", 30),
    ("Pair Programming vs Noise", 45),
    ("Rails Magic", 60),
    ("Ruby on Rails: Why We Should Move On", 60),
    ("Clojure Ate Scala (on my project)", 45),
    ("Programming in the Boondocks of Seattle", 30),
    ("Ruby vs. Clojure for Back-End Development", 30),
    ("Ruby on Rails Legacy App Maintenance", 60),
    ("A World Without HackerNews", 30),
    ("User Interface CSS in Rails Apps", 30),
]

# Checking if loaded directly from terminal.
if __name__ == '__main__':
    unittest.main()


##############################################################
##########################  Output  ##########################
##############################################################
"""
Track 1:
09:00AM Ruby on Rails Legacy App Maintenance
10:00AM Ruby on Rails: Why We Should Move On
11:00AM Rails Magic
12:00PM Lunch
01:00PM Communicating Over Distance
02:00PM Rails for Python Developers lightning
03:00PM Writing Fast Tests Against Enterprise Rails
04:00PM User Interface CSS in Rails Apps
04:30PM A World Without HackerNews
05:00PM Networking Event
Track 2:
09:00AM Clojure Ate Scala (on my project)
09:45AM Pair Programming vs Noise
10:30AM Accounting-Driven Development
11:15AM Common Ruby Errors
12:00PM Lunch
01:00PM Ruby Errors from Mismatched Gem Versions
01:45PM Overdoing it in Python
02:30PM Ruby vs. Clojure for Back-End Development
03:00PM Programming in the Boondocks of Seattle
03:30PM Sit Down and Write
04:00PM Woah
04:30PM Lua for the Masses
05:00PM Networking Event
----------------------------------------------------------------------
Ran 2 tests in 0.002s

OK

"""


# In case of any failure, the output would be:
"""
F
======================================================================
FAIL: testing_unfilled_sessions (__main__.Testing)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "sessionsandtracks.py", line 124, in testing_unfilled_sessions
    self.assertEqual(sum([t[1] for t in tracks[0]]), 181, "Track 1, before lunch is incomplete")
AssertionError: Track 1, before lunch is incomplete
"""
